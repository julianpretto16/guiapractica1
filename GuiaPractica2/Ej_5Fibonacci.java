package guiapractica2;

import java.util.Scanner;


public class Ej_5Fibonacci {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingresa el n�mero de t�rminos de la serie Fibonacci: ");
        int numTerminos = scanner.nextInt();
        
        System.out.println("La serie Fibonacci es:");
        
        int termino1 = 0;
        int termino2 = 1;
        
        System.out.print(termino1 + "\n" + termino2 + "\n");
        
        for (int i = 3; i <= numTerminos; i++) {
            int siguienteTermino = termino1 + termino2;
            System.out.print(siguienteTermino + "\n");
            
            termino1 = termino2;
            termino2 = siguienteTermino;
        }
    }
}
