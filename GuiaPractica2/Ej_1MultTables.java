package guiapractica2;

import java.util.Scanner;

public class Ej_1MultTables {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Introduce un numero: ");
        int num = s.nextInt();
        for (int i = 0; i < 10; i++) {
            System.out.printf("(%d x %d)\t %d\n", num, i, num * i);
        } 
    }
    
}
