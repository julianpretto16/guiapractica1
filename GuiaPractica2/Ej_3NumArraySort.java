package guiapractica2;

import java.util.Arrays;


public class Ej_3NumArraySort {
    public static void main(String[] args) {
        int[] arreglo = {2, 5, 4, 3, 9, 1, 23};
        System.out.println(Arrays.toString(arreglo));
        Arrays.sort(arreglo);
        System.out.println(Arrays.toString(arreglo));
    }
    
}
