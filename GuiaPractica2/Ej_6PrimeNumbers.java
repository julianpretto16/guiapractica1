package guiapractica2;

public class Ej_6PrimeNumbers {
    public static void main(String[] args) {
        for (int i = 2; i < 200; i++) {
            boolean primo = true;
            for (int j = 2; j <= Math.sqrt(i); j++) {
                if (i % j == 0) {
                    primo = false;
                    break;
                }
            }
            if (primo == true)
                System.out.print(i + "\n");
        }
    }
}
