package guiapractica2;

import java.util.Scanner;


public class Ej_4Factorial {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int num = s.nextInt();
        long factorial = 1;
        for (int i = 1; i <= num; i++) {
            factorial *= i;
        }
        System.out.printf("Factorial de %d = %d", num, factorial);
    }
}
