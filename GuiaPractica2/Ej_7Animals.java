package guiapractica2;

import java.util.Scanner;

public class Ej_7Animals {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        boolean herviboro = false;
        boolean mamifero = false;
        boolean domestico = false;

        System.out.print("�Es un herb�voro? (S/N): ");
        String entrada = scanner.next().toLowerCase();
        char respuestaHerbivoro = entrada.charAt(0);

        if (respuestaHerbivoro == 's') {
            herviboro = true;
        } else if (respuestaHerbivoro != 'n') {
            System.out.println("Respuesta inv�lida. Se asume \"no\"");
        }

        System.out.print("�Es un mam�fero? (S/N): ");
        entrada = scanner.next().toLowerCase();
        char respuestaMamifero = entrada.charAt(0);

        if (respuestaMamifero == 's') {
            mamifero = true;
        } else if (respuestaMamifero != 'n') {
            System.out.println("Respuesta inv�lida. Se asume \"no\"");
        }

        System.out.print("�Es un animal dom�stico? (S/N): ");
        entrada = scanner.next().toLowerCase();
        char respuestaDomestico = entrada.charAt(0);

        if (respuestaDomestico == 's') {
            domestico = true;
        } else if (respuestaDomestico != 'n') {
            System.out.println("Respuesta inv�lida. Se asume \"no\"");
        }
        
        if (herviboro && mamifero && !domestico) {
            System.out.println("El animal es un Alce.");
        } else if (herviboro && mamifero && domestico) {
            System.out.println("El animal es un Caballo.");
        } else if (herviboro && !mamifero && !domestico) {
            System.out.println("El animal es un Caracol.");
        } else if (!herviboro && !mamifero && !domestico) {
            System.out.println("El animal es un C�ndor.");
        } else if (!herviboro && mamifero && domestico) {
            System.out.println("El animal es un Gato.");
        } else if (!herviboro && mamifero && !domestico) {
            System.out.println("El animal es un Le�n.");
        } else if (!herviboro && !mamifero && domestico) {
            System.out.println("El animal es una Pit�n.");
        } else if (herviboro && !mamifero && domestico) {
            System.out.println("El animal es una Tortuga.");
        } else {
            System.out.println("No tengo idea che...");
        }
    }
}
