package guiapractica2;

import java.util.Scanner;

public class Ej_2Palindrome {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Introduce una palabra o frase: ");
        String palabra = s.nextLine();
        palabra = palabra.replaceAll("\\s+", "").toLowerCase();
        int longitud = palabra.length();
        for (int i = 0; i < longitud / 2; i++) {
            if (palabra.charAt(i) != palabra.charAt(longitud - 1 - i)) {
                System.out.print("La palabra o frase introducida no es un palindromo.");
                return;
            }
        }
        System.out.print("La palabra o frase introducida es un palindromo.");
    }
    
}
