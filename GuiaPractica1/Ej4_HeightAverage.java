package Guia1;

import java.util.Scanner;

public class Ej4_HeightAverage {
    public static void main(String[] args) {
        Scanner dataInput = new Scanner(System.in);

        System.out.print("Introduce la primera estatura: ");
        float estatura1 = dataInput.nextFloat();

        System.out.print("Introduce la segunda estatura: ");
        float estatura2 = dataInput.nextFloat();
        
        System.out.print("Introduce la tercera estatura: ");
        float estatura3 = dataInput.nextFloat();

        System.out.printf("Primera estatura: %.2f\nSegunda estatura: %.2f\nTercera estatura: %.2f\n", estatura1, estatura2, estatura3);

        float division = (estatura1 + estatura2 + estatura3) / 3;

        System.out.printf("Promedio de estaturas: %.2f\n", division); // El \n es para que el BUILD SUCCESSFUL no quede pegado
    }
}
