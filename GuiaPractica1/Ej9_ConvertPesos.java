package Guia1;

import java.util.Scanner;

public class Ej9_ConvertPesos {
    public static void main(String[] args) {
        Scanner dataInput = new Scanner(System.in);

        System.out.print("Introduce la cantidad de pesos: ");
        float pesos = dataInput.nextFloat();

        float dolares = pesos / 231.68f;
        float euros = pesos / 250.69f;
        float guaranies = pesos * 31.00f;
        float reales = pesos / 46.81f;

        System.out.printf("Pesos: %.2f\nDolares: %.2f\nEuros: %.2f\nGuaranies: %.2f\nReales: %.2f\n", pesos, dolares, euros, guaranies, reales);

    }
}
