package Guia1;

import java.util.Scanner;

public class Ej3_SimpleOperators {
    public static void main(String[] args) {
        Scanner dataInput = new Scanner(System.in);

        System.out.print("Introduce el primer n�mero: ");
        int numero1 = dataInput.nextInt();

        System.out.print("Introduce el segundo n�mero: ");
        int numero2 = dataInput.nextInt();

        System.out.printf("Primer n�mero: %d\nSegundo n�mero: %d\n", numero1, numero2);

        int suma = numero1 + numero2;
        int resta = numero1 - numero2;
        int multiplicacion = numero1 * numero2;
        double division = (double) numero1 / numero2;

        System.out.printf("Suma: %d\nResta: %d\nMultiplicaci�n: %d\nDivisi�n: %f\n", suma, resta, multiplicacion, division);

    }
}
