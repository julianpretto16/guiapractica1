package Guia1;

import java.util.Scanner;

public class Ej8_ConvertTemperatures {
    public static void main(String[] args) {
        Scanner dataInput = new Scanner(System.in);

        System.out.print("Introduce los grados celsius: ");
        float celsius = dataInput.nextFloat();
        
        float kelvin = 273.15f + celsius;
        float fahrenheit = 1.8f * celsius + 32;
        
        System.out.printf("Celsius: %.1f C\nKelvin: %.2f K\nFahrenheit: %.2f F\n", celsius, kelvin, fahrenheit); // El \n es para que el BUILD SUCCESSFUL no quede pegado
    }
}
