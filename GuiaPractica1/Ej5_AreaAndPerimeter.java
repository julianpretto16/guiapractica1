package Guia1;

import java.util.Scanner;

public class Ej5_AreaAndPerimeter {
    public static void main(String[] args) {
        Scanner dataInput = new Scanner(System.in);

        System.out.print("Introduce el radio de la circunferencia: ");
        float radio = dataInput.nextFloat();

        System.out.println("Radio: " + radio);
        
        double area = Math.PI * Math.pow(radio, 2);
        double perimetro = (2 * Math.PI * radio);
        
        System.out.printf("Area: %f\nPerimetro: %f", area, perimetro); // El \n es para que el BUILD SUCCESSFUL no quede pegado
    }
}
