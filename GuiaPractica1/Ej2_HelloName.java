package Guia1;

import java.util.Scanner;

public class Ej2_HelloName {
    public static void main(String[] args) {
        
        System.out.print("Como te llamas?\n");
        Scanner dataInput = new Scanner(System.in);
        
        String nombreEntrada = dataInput.nextLine();
        System.out.println("Hola, " + nombreEntrada);
    }
}
