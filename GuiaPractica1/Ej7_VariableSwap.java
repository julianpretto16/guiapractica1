package Guia1;

import java.util.Scanner;

public class Ej7_VariableSwap {
    public static void main(String[] args) {
        Scanner dataInput = new Scanner(System.in);

        System.out.print("La primera edad: ");
        int edad1 = dataInput.nextInt();
        
        System.out.print("La segunda edad: ");
        int edad2 = dataInput.nextInt();
        
        int variableTemporal = edad1;
        edad1 = edad2;
        edad2 = variableTemporal;
        
        System.out.printf("Variables cambiadas de lugar\nPrimera edad: %d\nSegunda edad: %d\n", edad1, edad2);
    }
}
