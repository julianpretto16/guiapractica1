package Guia1;

import java.util.Scanner;

public class Ej6_PriceDiscount {
    public static void main(String[] args) {
        Scanner dataInput = new Scanner(System.in);

        System.out.print("Introduce el precio original: ");
        float precioOriginal = dataInput.nextFloat();

        System.out.print("Introduce el porcentaje de descuento: ");
        float porcentajeDescuento = dataInput.nextFloat();

        float precioConDescuento = precioOriginal - (precioOriginal * (porcentajeDescuento / 100));

        System.out.printf("Precio con descuento: %.2f\n", precioConDescuento);
    }
}
